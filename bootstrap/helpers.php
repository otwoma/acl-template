<?php
/**
 * Created by PhpStorm.
 * User: godfrey
 * Date: 2020-04-18
 * Time: 13:35
 */


use Illuminate\Support\Facades\Auth;

if (!function_exists('user')) {
    /**
     *  Get currently logged in user
     *  use: user()->name or user('name')
     *
     * @param null $key
     * @return App\User
     */
    function user($key = null)
    {
        $user = \Illuminate\Support\Facades\Auth::user();

        if (!is_null($key))
            return isset($user->$key) ? $user->$key : null;

        return $user;
    }
}


if (!function_exists('logout_all_guards')) {

    function logout_all_guards()
    {
        $guards = array_keys(config('auth.guards'));
        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) Auth::guard($guard)->logout();
        }
    }
}
