@extends('layouts.app', [
    'namePage' => 'Roles',
    'class' => 'sidebar-mini',
    'activePage' => 'roles',
    'activeNav' => '',
])

@section('title', '| Roles')

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-default btn-round text-white pull-right" href="{{ route('users.index') }}" >Users</a>
                        <a class="btn btn-default btn-round text-white pull-right" href="{{ route('permissions.index') }}" >Permissions</a>
                        <a class="btn btn-primary btn-round text-white pull-right" href="{{ URL::to('roles/create') }}">{{ __('Add Role') }}</a>
                        <h4 class="card-title">{{ __('Roles') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="responsiveTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Role') }}</th>
                                <th>{{ __('Guard Name') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Actions') }}</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>{{ __('No') }}</th>
                                <th>{{ __('Role') }}</th>
                                <th>{{ __('Guard Name') }}</th>
                                <th class="disabled-sorting text-right">{{ __('Actions') }}</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach( $roles as $role )
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->guard_name }}</td>
                                    <td class="text-right">
                                        @if($role->id!=auth()->user()->id)
                                            <a type="button" href="{{route("roles.edit",$role)}}" rel="tooltip"
                                               class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </a>
                                            <form action="{{ route('roles.destroy', $role) }}" method="post"
                                                  style="display:inline-block;" class="delete-form">
                                                @csrf
                                                @method('delete')
                                                <button type="button" rel="tooltip"
                                                        class="btn btn-danger btn-icon btn-sm delete-button"
                                                        data-original-title="" title=""
                                                        onclick="confirm('{{ __('Are you sure you want to delete this role?') }}') ? this.parentElement.submit() : ''">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

@push('js')
    <script>
        $('#datatable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    </script>
    <script>
        $(function(){
            $('#responsiveTable').dataTable();
        });

    </script>
    @endpush