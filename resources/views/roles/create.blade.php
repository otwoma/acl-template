@extends('layouts.app', [
    'namePage' => 'Roles',
    'class' => 'sidebar-mini',
    'activePage' => 'roles',
    'activeNav' => '',
])

@section('title', '| Create Role')

@section('content')

    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Role Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('roles.index') }}" class="btn btn-primary btn-round">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('roles.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Role Creation') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">

                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}

                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>

                                <h5><b>Assign Permissions</b></h5>

                                <div class="form-group">
                                    @foreach($permissions as $permission)

                                        {{ Form::checkbox('permissions[]', $permission->id) }}
                                        {{ Form::label($permission->name, ucfirst($permission->name)) }}

                                    @endforeach

                                    @include('alerts.feedback', ['field' => 'email'])
                                </div>

                                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection