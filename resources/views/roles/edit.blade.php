@extends('layouts.app', [
    'namePage' => 'Roles',
    'class' => 'sidebar-mini',
    'activePage' => 'roles',
    'activeNav' => '',
])

@section('title', '| Update Role')

@section('content')

    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Role Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('roles.index') }}"
                                   class="btn btn-primary btn-round">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}
                            @csrf
                            @method('put')
                            <h6 class="heading-small text-muted mb-4">{{ __('Roles Management') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group">
                                    {{ Form::label('name', 'Role Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>

                                <h3>Assign Permissions</h3>

                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                    @foreach( $permissions as $permission )

                                        {{ Form::checkbox('permissions[]', $permission->id, $role->permissions) }}
                                        {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>

                                    @endforeach
                                    <br>

                                    {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

                                    {{ Form::close() }}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection