@extends('layouts.app', [
    'class' => 'sidebar-mini ',
    'namePage' => 'Create user',
    'activePage' => 'user',
    'activeNav' => '',
])

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('User Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('users.index') }}" class="btn btn-primary btn-round">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('url' => 'users')) !!}

                        <div class="form-group @if ($errors->has('name')) has-error @endif">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group @if ($errors->has('roles')) has-error @endif">
                            @foreach ($roles as $role)
                                {!! Form::checkbox('roles[]',  $role->id ) !!}
                                {!! Form::label($role->name, ucfirst($role->name)) !!}<br>

                            @endforeach
                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('password', 'Password') !!}<br>
                            {!! Form::password('password', array('class' => 'form-control')) !!}

                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('password', 'Confirm Password') !!}<br>
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}

                        </div>

                        {!! Form::submit('Add', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection