@extends('layouts.app', [
    'namePage' => 'Permissions',
    'class' => 'sidebar-mini',
    'activePage' => 'permissions',
    'activeNav' => '',
])

@section('title', '| Create Permission')

@section('content')

    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Permission Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('permissions.index') }}" class="btn btn-primary btn-round">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('permissions.store') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Permission Creation') }}</h6>
                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">

                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}

                                    @include('alerts.feedback', ['field' => 'name'])
                                </div>
                                @if(!$roles->isEmpty())
                                    <h4>Assign Permission to Roles</h4>
                                    <div class="form-group">
                                        @foreach ($roles as $role)
                                            {{ Form::checkbox('roles[]',  $role->id ) }}
                                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                                        @endforeach
                                    </div>
                                @endif

                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
