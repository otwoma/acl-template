@extends('layouts.app', [
    'namePage' => 'Permissions',
    'class' => 'sidebar-mini',
    'activePage' => 'permissions',
    'activeNav' => '',
])

@section('title', '| Permissions')

@section('content')
    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-default btn-round text-white pull-right" href="{{ route('users.index') }}" >Users</a>
                        <a class="btn btn-default btn-round text-white pull-right" href="{{ route('roles.index') }}" >Roles</a>
                        <a class="btn btn-primary btn-round text-white pull-right" href="{{ URL::to('permissions/create') }}">{{ __('Add Permission') }}</a>
                        <h4 class="card-title">{{ __('Permission') }}</h4>
                        <div class="col-12 mt-2">
                            @include('alerts.success')
                            @include('alerts.errors')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Permissons</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Permissons</th>
                                <th class="disabled-sorting text-right">Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($permissions as $permission)

                                <tr>
                                    <td>{{ $permission->id }}</td>
                                    <td>{{ $permission->name }}</td>
                                    <td class="text-right">
                                        @if($permission->id!=auth()->user()->id)
                                            <a type="button" href="{{route("permissions.edit",$permission)}}" rel="tooltip"
                                               class="btn btn-success btn-icon btn-sm " data-original-title="" title="">
                                                <i class="now-ui-icons ui-2_settings-90"></i>
                                            </a>
                                            <form action="{{ route('permissions.destroy', $permission) }}" method="post"
                                                  style="display:inline-block;" class="delete-form">
                                                @csrf
                                                @method('delete')
                                                <button type="button" rel="tooltip"
                                                        class="btn btn-danger btn-icon btn-sm delete-button"
                                                        data-original-title="" title=""
                                                        onclick="confirm('{{ __('Are you sure you want to delete this role?') }}') ? this.parentElement.submit() : ''">
                                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@push('js')
    <script>
        $('#datatable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    </script>
@endpush

