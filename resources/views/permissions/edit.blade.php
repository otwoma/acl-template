@extends('layouts.app', [
    'namePage' => 'Permissions',
    'class' => 'sidebar-mini',
    'activePage' => 'permissions',
    'activeNav' => '',
])

@section('title', '| Edit Permission')

@section('content')

    <div class="panel-header panel-header-sm">
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Permission Management') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('permissions.index') }}"
                                   class="btn btn-primary btn-round">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}
                        @csrf
                        @method('put')
                        <h6 class="heading-small text-muted mb-4">{{ __('Permissions Management') }}</h6>
                        <div class="pl-lg-4">
                            <div class="form-group">
                                {{ Form::label('name', 'Permission Name') }}
                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                            </div>

                                {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

                                {{ Form::close() }}
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection