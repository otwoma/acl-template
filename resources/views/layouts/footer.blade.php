<footer class="footer">
  <div class=" container-fluid ">
    <nav>
      <ul>
        <li>
         <p>ACL Template</p>
        </li>
      </ul>
    </nav>
    <div class="copyright" id="copyright">
      &copy;
      <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script>
    </div>
  </div>
</footer>