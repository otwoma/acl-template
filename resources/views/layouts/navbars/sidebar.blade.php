<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-normal">
            {{ __('ACL TEMPLATE') }}
        </a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
            <li class="@if ($activePage == 'home') active @endif">
                <a href="{{ route('home') }}">
                    <i class="now-ui-icons design_app"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#laravelExamples">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>
                        {{ __("User Management") }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse show" id="laravelExamples">
                    <ul class="nav">
                        <li class="@if ($activePage == 'permissions') active @endif">
                            <a href="{{ route('permissions.index', 'permissions') }}">
                                <i class="now-ui-icons gestures_tap-01"></i>
                                <p> {{ __("Permissions") }} </p>
                            </a>
                        </li>
                        <li class="@if ($activePage == 'roles') active @endif">
                            <a href="{{ route('roles.index', 'roles') }}">
                                <i class="now-ui-icons objects_key-25"></i>
                                <p> {{ __("Roles") }} </p>
                            </a>
                        </li>
                        <li class="@if ($activePage == 'users') active @endif">
                            <a href="{{ route('users.index', 'users') }}">
                                <i class="now-ui-icons users_single-02"></i>
                                <p> {{ __("Users") }} </p>
                            </a>
                        </li>
                    </ul>
                </div>
            <li class="@if ($activePage == 'reports') active @endif">
                <a href="#">
                    <i class="now-ui-icons education_atom"></i>
                    <p>{{ __('Reports') }}</p>
                </a>
            </li>

            <li class="">
                <a href="{{route('logout')}}">
                    <i class="now-ui-icons media-1_button-power"></i>
                    <p>{{ __('Logout') }}</p>
                </a>
            </li>



        </ul>
    </div>
</div>