<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    use AuthenticatesUsers;


    public function login()
    {

        return view('login');
    }

    public function loginProcess(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required'
        ]);

       // dd($request->all());

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {

            $user = user();

            flash('Welcome')->success();

            return  redirect()->intended(route('home'));
        }


        flash('Incorrect Credentials')->error();
        return redirect()->back()->withInput();


    }




    public function logout()
    {

        logout_all_guards();

        flash(__("Successfully logged out"))->info();

        return redirect()->route('login');
    }


}
